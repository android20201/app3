package com.example.app3

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.app3.model.Oil


class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val itemList : List<Oil> = listOf(
            Oil(36.84,"เชลล์ ฟิวเซฟ แก๊สโซฮอล์ E20"),
            Oil(37.68,"เชลล์ ฟิวเซฟ แก๊สโซฮอล์ 91"),
            Oil(37.95,"เชลล์ ฟิวเซฟ แก๊สโซฮอล์ 95"),
            Oil(45.95,"เชลล์ วี-พาวเวอรื แก๊สโซฮอล์ 95"),
            Oil(36.34,"เชลล์ ดีเซล B20"),
            Oil(36.34,"เชลล์ ฟิวเซฟ ดีเซล"),
            Oil(36.34,"เชลล์ ฟิวเซฟ ดีเซล B7"),

        )


        val recyclerView = findViewById<RecyclerView>(R.id.recyclerView)
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = ItemAdpter(itemList)
        var adapters = ItemAdpter(itemList)
        recyclerView.adapter = adapters
        adapters.setOnItemClickListener(object : ItemAdpter.onItemClickListener {
            override fun onItemClick(position: Int) {
                Toast.makeText(
                    this@MainActivity,
                    itemList[position].price.toString() + "     " + itemList[position].name,
                    Toast.LENGTH_LONG
                ).show()
            }
        })

    }

    class ItemAdpter(val oil : List<Oil>) : RecyclerView.Adapter<ItemAdpter.ViewHolder>() {
        private lateinit var mListener: onItemClickListener
        interface onItemClickListener {
            fun onItemClick(position: Int)
        }

        fun setOnItemClickListener(listener: onItemClickListener) {
            mListener = listener
        }


        class ViewHolder(private val itemView : View, listener: onItemClickListener) : RecyclerView.ViewHolder(itemView){
            val price = itemView.findViewById<TextView>(R.id.text_price)
            val name = itemView.findViewById<TextView>(R.id.text_name)
            init {
                itemView.setOnClickListener {
                    listener.onItemClick(adapterPosition)
                }
            }

        }


        // setLayout ของ recycler
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemAdpter.ViewHolder {
            val adapterLayout = LayoutInflater.from(parent.context).inflate(R.layout.item , parent ,false)
            return ViewHolder(adapterLayout, mListener)
        }


        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            holder.price.text = oil[position].price.toString()
            holder.name.text = oil[position].name

        }


        override fun getItemCount(): Int {
            return oil.size
        }



    }


}
