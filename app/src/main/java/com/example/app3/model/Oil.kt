package com.example.app3.model

import android.annotation.SuppressLint

import androidx.annotation.StringRes

data class Oil (val  price : Double, val name : String) {
}
